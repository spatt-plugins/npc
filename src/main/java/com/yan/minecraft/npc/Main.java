package com.yan.minecraft.npc;

import com.google.common.collect.Lists;
import com.yan.minecraft.npc.command.NpcCommand;
import com.yan.minecraft.npc.util.CustomPlayer;
import com.yan.minecraft.npc.util.MasterFile;
import com.yan.minecraft.npc.util.NPC;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.UUID;

public final class Main extends JavaPlugin {

    private static MasterFile configuration;
    private List<NPC> npcList = Lists.newArrayList();

    @Override
    public void onEnable() {
        configuration = new MasterFile(this,"config.yml");
        loadNpcList();
        ((CraftServer)getServer()).getCommandMap().register("",new NpcCommand());

    }

    @Override
    public void onDisable() {
        getNpcList().forEach(NPC::destroy);
    }

    public static MasterFile getConfiguration() {
        return configuration;
    }

    public List<NPC> getNpcList() {
        return npcList;
    }

    private void loadNpcList(){
        ConfigurationSection section = getConfiguration().getConfigurationSection("");
        if (section != null){
            for (String key : section.getKeys(false)){
                NPC npc = new NPC(getConfiguration().getString(key + ".name"), UUID.fromString(key));
                npc.spawn(getConfiguration().getLocation(key + ".location"));
                npc.animate();
            }
        }
    }
}
