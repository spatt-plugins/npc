package com.yan.minecraft.npc.util;

import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_8_R3.*;

import java.lang.reflect.Field;

public class CustomPlayer extends EntityPlayer {

    public CustomPlayer(MinecraftServer minecraftServer, WorldServer worldServer, GameProfile gameProfile) {
        super(minecraftServer, worldServer, gameProfile, new PlayerInteractManager(worldServer));
    }

    public static Object getPrivateField(String fieldName, Class clazz, Object object) {
        Field field;
        Object o = null;
        try
        {
            field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            o = field.get(object);
        }
        catch(NoSuchFieldException e)
        {
            e.printStackTrace();
        }
        catch(IllegalAccessException e)
        {
            e.printStackTrace();
        }
        return o;
    }

}