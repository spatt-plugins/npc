package com.yan.minecraft.npc.util;

import com.mojang.authlib.GameProfile;
import com.yan.minecraft.npc.Main;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.util.UUID;

public class NPC {

    CustomPlayer entityPlayer;
    String name;
    Location location;
    UUID uuid;

    public NPC(String name, UUID uuid){
        this.name = name;
        this.uuid = uuid;
    }

    public NPC(String name) {
        this(name, UUID.randomUUID());
    }

    public void spawn(Location location) {
        this.location = location;
        MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();

        WorldServer worldServer = ((CraftWorld) location.getWorld()).getHandle();
        GameProfile gameProfile = new GameProfile(uuid, this.name);

        entityPlayer = new CustomPlayer(nmsServer, worldServer, gameProfile);
        Main.getPlugin(Main.class).getNpcList().add(this);

        Player npcPlayer = entityPlayer.getBukkitEntity().getPlayer();
        npcPlayer.setPlayerListName("");

        entityPlayer.setLocation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());

        Bukkit.getOnlinePlayers().forEach(player -> {
            PlayerConnection playerConnection = ((CraftPlayer) player).getHandle().playerConnection;

            playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, entityPlayer));
            playerConnection.sendPacket(new PacketPlayOutNamedEntitySpawn(entityPlayer));
            playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, entityPlayer));
        });
    }

    public void destroy() {

        MasterFile file = Main.getConfiguration();

        file.set(getUuid().toString() + ".name",getName());
        file.setLocation(getUuid().toString() + ".location",getLocation());

        file.save();

        PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(entityPlayer.getId());
        Bukkit.getOnlinePlayers().forEach(player -> {
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(destroy);
        });
    }

    public void animate(){
        new BukkitRunnable(){
            int index = 0;
            @Override
            public void run() {
                if(index == 0) {
                    jump();
                } else if(index == 1) {
                    jump();
                }else if (index == 2){
                    sneak( true);
                }else if (index == 3){
                    sneak( false);
                }else if (index == 4){
                    sneak( true);
                }else if (index == 5){
                    Bukkit.getOnlinePlayers().forEach(player -> {
                        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutAnimation(entityPlayer, 0));
                    });
                }else if (index == 6){
                    sneak( false);
                } else {
                    cancel();
                }
                index++;
            }
        }.runTaskTimer(Main.getPlugin(Main.class), 20L, 20L);
    }

    public void jump() {
        walk(0, 1, 0, 0, 0, entityPlayer.getId(), true);
    }
    public void sneak( boolean b){
        DataWatcher dw = new DataWatcher(entityPlayer);
        dw.a(0, (byte) (b ? 2 : 0));
        Bukkit.getOnlinePlayers().forEach(player -> {
            ((CraftPlayer)player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(entityPlayer.getId(), dw, true));
        });
    }

    private void setPrivateField(@SuppressWarnings("rawtypes") Class type, Object object, String name, Object value) {
        try {
            Field f = type.getDeclaredField(name);
            f.setAccessible(true);
            f.set(object, value);
            f.setAccessible(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void walk(double a, double b, double c, float yaw, float pitch, int id, boolean d) {
        byte x = (byte) (a * 32);
        byte y = (byte) (b * 32);
        byte z = (byte) (c * 32);

        PacketPlayOutEntity.PacketPlayOutRelEntityMoveLook packet = new PacketPlayOutEntity.PacketPlayOutRelEntityMoveLook();
        setPrivateField(PacketPlayOutEntity.class, packet, "a", id);
        setPrivateField(PacketPlayOutEntity.class, packet, "b", x);
        setPrivateField(PacketPlayOutEntity.class, packet, "c", y);
        setPrivateField(PacketPlayOutEntity.class, packet, "d", z);
        PacketPlayOutEntityHeadRotation p2 = new PacketPlayOutEntityHeadRotation();
        setPrivateField(PacketPlayOutEntityHeadRotation.class, p2, "a", id);
        for (Player p : Bukkit.getOnlinePlayers()) {
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(p2);
        }

        if(d) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    walk(a, b*-1, c, yaw, pitch, id, false);
                }
            }.runTaskLater(Main.getPlugin(Main.class), 10L);
        }
    }

    public CustomPlayer getEntityPlayer() {
        return entityPlayer;
    }

    public void setEntityPlayer(CustomPlayer entityPlayer) {
        this.entityPlayer = entityPlayer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}