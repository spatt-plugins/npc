package com.yan.minecraft.npc.command;

import com.yan.minecraft.npc.util.NPC;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NpcCommand extends Command {

    public NpcCommand(){
        super("createnpc");
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] arguments) {
        if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (arguments.length == 0){
                player.sendMessage(ChatColor.RED +"Sintax: /createnpc <name>");
                return true;
            }
            NPC npc = new NPC(ChatColor.translateAlternateColorCodes('&',arguments[0]));
            npc.spawn(player.getLocation());
            npc.animate();
            player.sendMessage(ChatColor.GREEN + "Summoned NPC");
        }else{
            commandSender.sendMessage(ChatColor.RED +"Only for players");
        }
        return false;
    }
}
